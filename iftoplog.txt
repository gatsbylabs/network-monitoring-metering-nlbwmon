Listening on enp0s3
   # Host name (port/service if enabled)            last 2s   last 10s   last 40s cumulative
--------------------------------------------------------------------------------------------
   1 sunny-VirtualBox                         =>         0b     7.00Kb     5.64Kb     61.1KB
     74.125.172.55                            <=         0b     1.23Mb     1.09Mb     9.99MB
   2 sunny-VirtualBox                         =>     2.18Kb     1.13Kb       756b     11.7KB
     lga34s11-in-f14.1e100.net                <=     1.10Kb       492b       308b     5.59KB
   3 sunny-VirtualBox                         =>         0b       153b        86b     2.07KB
     server-52-85-93-5.jfk6.r.cloudfront.net  <=         0b       128b        80b     4.96KB
   4 sunny-VirtualBox                         =>         0b       101b        25b       252B
     172.217.12.150                           <=         0b       101b        25b       252B
   5 sunny-VirtualBox                         =>         0b         0b        24b     2.85KB
     ec2-35-162-31-77.us-west-2.compute.amaz  <=         0b         0b        38b     23.0KB
   6 sunny-VirtualBox                         =>         0b         0b        16b       480B
     63.130.77.46                             <=         0b         0b        24b       520B
--------------------------------------------------------------------------------------------
Total send rate:                                     2.18Kb     8.38Kb     6.53Kb
Total receive rate:                                  1.10Kb     1.23Mb     1.09Mb
Total send and receive rate:                         3.27Kb     1.24Mb     1.09Mb
--------------------------------------------------------------------------------------------
Peak rate (sent/received/total):                     35.4Kb     6.19Mb     6.22Mb
Cumulative (sent/received/total):                     121KB     12.6MB     12.7MB
============================================================================================


Note: 
1. Use open source program iftop to measure bandwidth of Gatsby Router with Wireguard VPN
    a. iftop url: http://www.ex-parrot.com/pdw/iftop/ 
    b. wireguard url: https://www.wireguard.com/